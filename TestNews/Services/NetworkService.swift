//
//  NetworkService.swift
//  TestNews
//
//  Created by macOS on 26.03.21.
//

import Foundation
import Alamofire

class NetworkService {
    private static let apiKey = "eadd99dd82dd31fb83f8dce4c60dd4ae"
    private static let base = "http://api.mediastack.com/v1/"

    class func getNews(_ offsetFromToday: Int = 0, completion: @escaping(Result<[News], Error>) -> Void) {

        var params: [String: AnyHashable] = [
            "access_key": apiKey,
            "languages": "en",
            "countries": "us"
        ]

        if offsetFromToday != 0 {
            let today = Date()
            let calendar = Calendar.current
            let components = DateComponents(day: -offsetFromToday)
            guard let targetDay = calendar.date(byAdding: components, to: today) else { fatalError() }
            let formatter = DateFormatter()
            formatter.dateFormat = "yyy-MM-dd"
            params["date"] = formatter.string(from: targetDay)
        }

        struct Model: Decodable {
            let data: [News]
        }

        let url = base + "news"
        AF.request(url, method: .get, parameters: params)
            .responseDecodable(of: Model.self) { response in
                switch response.result {
                case .success(let model):
                    completion(.success(model.data))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}
