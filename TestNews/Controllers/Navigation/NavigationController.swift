//
//  NavigationController.swift
//  TestNews
//
//  Created by macOS on 1.04.21.
//

import UIKit

class NavigationController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let root = Builder.buildListController()
        setViewControllers([root], animated: false)
    }
}
