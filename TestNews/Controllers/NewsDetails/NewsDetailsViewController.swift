//
//  NewsDetailsViewController.swift
//  TestNews
//
//  Created by macOS on 31.03.21.
//

import UIKit
import Hero
import Kingfisher

class NewsDetailsViewController: UIViewController {

    var viewModel: NewsDetailsViewModelType!

    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!

    var news: News?

    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.heroID = "imageView"
        titleLabel.heroID = "title"
        authorLabel.heroID = "author"
        contentLabel.heroID = "content"

        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.cornerRadius = 6
        imageView.layer.masksToBounds = true

        navigationItem.setRightBarButton(UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(openInSafari)), animated: true)

        setup()
    }
    
    func setup() {
        if let url = viewModel.output.imageUrl {
            imageView.kf.setImage(with: url)
        } else {
            imageView.image = nil
        }

        authorLabel.isHidden = (viewModel.output.author ?? "").isEmpty

        titleLabel.text = viewModel.output.title
        authorLabel.text = viewModel.output.author
        contentLabel.text = viewModel.output.content
    }

    @objc private func openInSafari() {
        viewModel.input.onOpenInSafari()
    }

}
