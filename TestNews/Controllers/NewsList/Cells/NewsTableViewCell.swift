//
//  NewsTableViewCell.swift
//  TestNews
//
//  Created by macOS on 30.03.21.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var showMoreLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        newsImageView.contentMode = .scaleAspectFill
        newsImageView.layer.borderWidth = 1
        newsImageView.layer.borderColor = UIColor.lightGray.cgColor
        newsImageView.layer.cornerRadius = 6
        newsImageView.layer.masksToBounds = true
    }

    func setup(_ news: News) {
        if let url = news.image {
            newsImageView.kf.setImage(with: url)
        } else {
            newsImageView.image = nil
        }

        subtitleLabel.isHidden = (news.author ?? "").isEmpty

        titleLabel.text = news.title
        subtitleLabel.text = news.author
        bodyLabel.text = news.description

        showMoreLabel.isHidden = titleLabel.calculateMaxLines() < 3
    }
    
}
