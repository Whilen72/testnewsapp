//
//  NewsViewController.swift
//  TestNews
//
//  Created by macOS on 30.03.21.
//

import UIKit
import PullToRefreshKit
import RxSwift
import Hero

class NewsViewController: UIViewController {

    // MARK: - Interface

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: String(describing: NewsTableViewCell.self), bundle: nil), forCellReuseIdentifier: NewsTableViewCell.kReuseIdentifier)
        }
    }

    var viewModel: NewsViewModelType!
    private let disposeBag = DisposeBag()

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "News list"

        hero.isEnabled = true
        navigationController?.hero.isEnabled = true

        setupViewModel()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) { [weak self] in
            self?.tableView.visibleCells.compactMap { $0 as? NewsTableViewCell }.forEach { (cell) in
                cell.newsImageView.heroID = nil
                cell.titleLabel.heroID = nil
                cell.subtitleLabel.heroID = nil
                cell.bodyLabel.heroID = nil
            }
        }
    }

    // MARK: - Methods

    private func setupViewModel() {
        viewModel.output.searchText.subscribe(onNext: { [weak self] text in
            DispatchQueue.main.async {
                self?.searchBar.text = text
            }
        }).disposed(by: disposeBag)
        viewModel.output.news.subscribe(onNext: { [weak self] _ in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }).disposed(by: disposeBag)
        viewModel.output.headerRefresherState.subscribe(onNext: { [weak self] state in
            DispatchQueue.main.async {
                self?.tableView.switchRefreshHeader(to:state)
            }
        }).disposed(by: disposeBag)
        viewModel.output.footerRefresherState.subscribe(onNext: { [weak self] state in
            DispatchQueue.main.async {
                self?.tableView.switchRefreshFooter(to: state)
            }
        }).disposed(by: disposeBag)
        viewModel.output.onAddHeaderRefresher.observe(on: MainScheduler.asyncInstance).subscribe(onNext: { [weak self] in
            DispatchQueue.main.async {
                self?.addHeaderRefresher()
            }
        }).disposed(by: disposeBag)
        viewModel.output.onAddFooterRefresher.observe(on: MainScheduler.asyncInstance).subscribe(onNext: { [weak self] in
            DispatchQueue.main.async {
                self?.addFooterRefresher()
            }
        }).disposed(by: disposeBag)
        viewModel.output.onShowError.subscribe(onNext: { [weak self] message in
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                alert.addAction(.init(title: "ON", style: .destructive))
                self?.show(alert, sender: nil)
            }
        }).disposed(by: disposeBag)

        viewModel.input.controllerDidSetup()
    }

    private func addHeaderRefresher() {
        tableView.configRefreshHeader(container: view) { [weak self] in
            self?.viewModel.input.onReloadContent()
        }
    }

    private func addFooterRefresher() {
        tableView.configRefreshFooter(container: view) { [weak self] in
            self?.viewModel.input.onLoadNewsForNextDay()
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.resignFirstResponder()
    }
}

extension NewsViewController: UIScrollViewDelegate {

}

extension NewsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let cell = tableView.cellForRow(at: indexPath) as! NewsTableViewCell

        cell.newsImageView.heroID = "imageView"
        cell.titleLabel.heroID = "title"
        cell.subtitleLabel.heroID = "author"
        cell.bodyLabel.heroID = "content"

        let news = (try! viewModel.output.news.value())[indexPath.row]
        let controller = Builder.buildDetailsController(news)

        navigationController?.pushViewController(controller, animated: true)
    }
}

extension NewsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int { 1 }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { (try! viewModel.output.news.value()).count }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.kReuseIdentifier, for: indexPath) as! NewsTableViewCell

        let news = (try! viewModel.output.news.value())[indexPath.row]
        cell.setup(news)
        return cell
    }
}

extension NewsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.input.onSearchInput(searchText)
    }
}
