//
//  NewsDetailsViewModel.swift
//  TestNews
//
//  Created by macOS on 1.04.21.
//

import UIKit
import RxSwift
import PullToRefreshKit

protocol NewsDetailsViewModelInput {
    func onOpenInSafari()
}

protocol NewsDetailsViewModelOutput {
    var title: String? { get }
    var author: String? { get }
    var content: String? { get }
    var imageUrl: URL? { get }
}

protocol NewsDetailsViewModelType {
    var input: NewsDetailsViewModelInput { get }
    var output: NewsDetailsViewModelOutput { get }
}

class NewsDetailsViewModel: NewsDetailsViewModelType, NewsDetailsViewModelInput, NewsDetailsViewModelOutput {

    // MARK: - Interface

    var input: NewsDetailsViewModelInput { self }
    var output: NewsDetailsViewModelOutput { self }

    let title: String?
    let author: String?
    let content: String?
    let imageUrl: URL?

    private let news: News

    init(_ news: News) {
        self.news = news

        title = news.title
        author = news.author
        content = news.description
        imageUrl = news.image
    }

    // MARK: - NewsViewModelInput

    func onOpenInSafari() {
        if let url = news.url {
            UIApplication.shared.open(url)
        }
    }
}
