//
//  NewsViewModel.swift
//  TestNews
//
//  Created by macOS on 30.03.21.
//

import Foundation
import RxSwift
import PullToRefreshKit

protocol NewsViewModelInput {
    func controllerDidSetup()
    func onReloadContent()
    func onLoadNewsForNextDay()
    func onSearchInput(_ input: String)
}

protocol NewsViewModelOutput {
    var news: BehaviorSubject<[News]> { get }
    var footerRefresherState: PublishSubject<FooterRefresherState> { get }
    var headerRefresherState: PublishSubject<HeaderRefresherState> { get }
    var onAddHeaderRefresher: PublishSubject<Void> { get }
    var onAddFooterRefresher: PublishSubject<Void> { get }
    var onShowError: PublishSubject<String> { get }
    var searchText: PublishSubject<String?> { get }
}

protocol NewsViewModelType {
    var input: NewsViewModelInput { get }
    var output: NewsViewModelOutput { get }
}

class NewsViewModel: NewsViewModelType, NewsViewModelInput, NewsViewModelOutput {

    // MARK: - Interface

    var input: NewsViewModelInput { self }
    var output: NewsViewModelOutput { self }

    let headerRefresherState = PublishSubject<HeaderRefresherState>()
    let footerRefresherState = PublishSubject<FooterRefresherState>()
    let onShowError = PublishSubject<String>()
    let onAddHeaderRefresher = PublishSubject<Void>()
    let onAddFooterRefresher = PublishSubject<Void>()
    let searchText = PublishSubject<String?>()

    let news = BehaviorSubject<[News]>(value: [])
    var allNews: [News] = []

    private var isLoadInProgress: Bool = false
    private let maxButchesCount = 5
    private var numberOfLoadedBatches: Int = 0 {
        didSet {
            switch numberOfLoadedBatches {
            case maxButchesCount:
                footerRefresherState.onNext(.removed)
            default:
                break
            }
        }
    }

    // MARK: - NewsViewModelInput

    func controllerDidSetup() {
        NetworkService.getNews { [weak self] (result) in
            switch result {
            case .success(let news):
                let allNews = (self?.allNews ?? []) + news
                self?.allNews = allNews
                self?.news.onNext(allNews)
                self?.onAddHeaderRefresher.onNext(())
                self?.onAddFooterRefresher.onNext(())
                self?.numberOfLoadedBatches += 1
            case .failure(let error):
                self?.onShowError.onNext(error.localizedDescription)
            }
        }
    }

    func onReloadContent() {
        allNews.removeAll()
        numberOfLoadedBatches = 0
        footerRefresherState.onNext(.removed)
        
        NetworkService.getNews { [weak self] (result) in
            self?.onAddFooterRefresher.onNext(())
            switch result {
            case .success(let news):
                let allNews = (self?.allNews ?? []) + news
                self?.allNews = allNews
                self?.news.onNext(allNews)
                self?.headerRefresherState.onNext(.normal(.success, 0))
                self?.footerRefresherState.onNext(.normal)
                self?.numberOfLoadedBatches += 1
            case .failure(let error):
                self?.headerRefresherState.onNext(.normal(.failure, 0))
                self?.onShowError.onNext(error.localizedDescription)
            }
        }
    }

    func onLoadNewsForNextDay() {
        if !isLoadInProgress {
            searchText.onNext(nil)
            isLoadInProgress = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self, numberOfLoadedBatches] in
                NetworkService.getNews(numberOfLoadedBatches) { [weak self] (result) in
                    switch result {
                    case .success(let news):
                        let allNews = (self?.allNews ?? []) + news
                        self?.allNews = allNews
                        self?.news.onNext(allNews)
                        self?.numberOfLoadedBatches += 1
                    case .failure(let error):
                        self?.onShowError.onNext(error.localizedDescription)
                    }
                    self?.footerRefresherState.onNext(.normal)
                    self?.isLoadInProgress = false
                }
            }
        }
    }

    func onSearchInput(_ input: String) {
        if input.isEmpty {
            news.onNext(allNews)
        } else {
            let newsToDisplay = allNews.filter({ ($0.title ?? "").contains(input) })
            news.onNext(newsToDisplay)
        }
    }
}
