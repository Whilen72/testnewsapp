//
//  UITableViewCell+Methods.swift
//  TestNews
//
//  Created by macOS on 29.03.21.
//

import UIKit

extension UITableViewCell {
    static var kReuseIdentifier: String { "k" + String(describing: Self.self) + "ReuseIdentifier" }
}
