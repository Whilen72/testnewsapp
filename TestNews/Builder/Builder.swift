//
//  Builder.swift
//  TestNews
//
//  Created by macOS on 1.04.21.
//

import UIKit
import Hero

class Builder {
    class func buildListController() -> NewsViewController {
        let controller = UIStoryboard(name: String(describing: NewsViewController.self), bundle: nil).instantiateInitialViewController() as! NewsViewController
        controller.viewModel = NewsViewModel()
        controller.hero.isEnabled = true
        return controller
    }

    class func buildDetailsController(_ news: News) -> NewsDetailsViewController {
        let controller = UIStoryboard(name: String(describing: NewsDetailsViewController.self), bundle: nil).instantiateInitialViewController() as! NewsDetailsViewController
        controller.hero.isEnabled = true
        controller.viewModel = NewsDetailsViewModel(news)
        return controller
    }
}
