//
//  News.swift
//  TestNews
//
//  Created by macOS on 26.03.2021.
//

import Foundation

struct News: Decodable {
    let author: String?
    let title: String?
    let description: String?
    let image: URL?
    let url: URL?
}
